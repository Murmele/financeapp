#!/bin/bash
# reconfigure meson build. This must be done whenever the Cargo.toml.in was changed
cd builddir
ninja reconfigure
