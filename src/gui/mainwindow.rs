
use iced::{Sandbox, Element, Alignment};
use iced::widget::{column, text};

pub struct MainWin {

}

#[derive(Debug, Clone, Copy)]
pub enum Message {

}

impl Sandbox for MainWin {
  type Message = Message;

  fn new() -> Self {
    Self {}
  }

  fn title(&self) -> String {
    String::from("Finance App")
  }

  fn update(&mut self, _message:Message) {

  }

  fn view(&self) -> Element<Message> {
    column![
      text("Hello World").size(50)
    ].padding(20).align_items(Alignment::Center).into()
  }
}