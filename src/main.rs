use iced::{Sandbox, Settings};

mod gui;
use gui::mainwindow::MainWin;

fn main() -> iced::Result {
  MainWin::run(Settings::default())
}
